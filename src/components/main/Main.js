import React, { Component } from 'react';
import PresentationalComponent from './PresentationalComponent';

class Main extends Component {
    state = {
        phoneNumber: this.props.phoneNumber,
        menuItems: []
    };

    displayPhoneNumber() {
        alert(this.state.phoneNumber);
    }



    render() {
        return (
            <PresentationalComponent
                {...this.state}
                displayPhoneNumber={this.displayPhoneNumber.bind(this)}
            ></PresentationalComponent>
        );
    }
}

export default Main;