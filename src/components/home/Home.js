import React, { Component } from 'react';
import PresentationalComponent from './PresentationalComponent';
import { Actions } from 'react-native-router-flux';
import { Main } from '../main/Main'

class Home extends Component {
    state = {
        messageStatus: "",
        phoneNumber: ""
    }
    changeInput(text) {
        console.log(text);
        this.state.phoneNumber=text;
    }
    updateState = () => {
        if (this.state.phoneNumber == 'aaa') {
            alert("Success logined");
            Actions.jump('main',{phoneNumber: this.state.phoneNumber});
        }
        else {
            this.setState({ messageStatus: "Error: Please, check your number" });
        }
    }
    render() {
        return (
                <PresentationalComponent
                    {...this.state}
                    logIn = {this.updateState}
                    changeInput = {this.changeInput.bind(this)}
                ></PresentationalComponent>
        );
    }
}

export default Home;