/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { Router, Scene } from 'react-native-router-flux';
import Home from './src/components/home/Home'
import Main from './src/components/main/Main'
import StyleSheet from 'react-native'

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <Router>
        <Scene key="root">
          <Scene key="home"
            component={Home}
            hideNavBar
          />
          <Scene
            key="main"
            component={Main}
            initial
            hideNavBar
          />
        </Scene>
      </Router>

    );

  }
}
